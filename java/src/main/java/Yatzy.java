import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Yatzy {

    protected ArrayList<Integer> dice;

    int maxScoreOnDice = 6;
    int minScoreOnDice = 1;

    public Yatzy(int d1, int d2, int d3, int d4, int d5)
    {
        dice = new ArrayList<>();
        dice.add(d1);
        dice.add(d2);
        dice.add(d3);
        dice.add(d4);
        dice.add(d5);
    }

    public int chance()
    {
        int total = 0;

        for (int d : dice) {
            total += d;
        }

        return total;
    }

    public int byNumber(int numberToMatch) {
        List<Integer> result = dice.stream().filter(d -> d == numberToMatch).collect(Collectors.toList());

        return result.size() * numberToMatch;
    }

    public int scoreMultiples(int NumberOfOccurences)
    {
        int highestPair = 0;

        for (int i = minScoreOnDice; i <= maxScoreOnDice; i++) {
            final int numberToTest = i;
            List<Integer> numberOfOccurence = dice.stream().filter(d -> d == numberToTest).collect(Collectors.toList());
            if (numberOfOccurence.size() >= NumberOfOccurences) {
                highestPair = i;
            }
        }

        return highestPair * NumberOfOccurences;
    }

    public int two_pair()
    {

        int highestPair = testForHighestPair(maxScoreOnDice, dice);
        int secondHighestPair = testForHighestPair(highestPair-1, dice);

        return (highestPair * 2) + (secondHighestPair * 2);
    }

    private int testForHighestPair(int limitMax, List<Integer> listToTest) {
        int result = 0;
        for (int i = minScoreOnDice; i <= limitMax; i++) {
            final int numberToTest = i;
            List<Integer> numberOfOccurence = listToTest.stream().filter(d -> d == numberToTest).collect(Collectors.toList());
            if (numberOfOccurence.size() >= 2) {
                result = i;
            }
        }

        return result;
    }

    public int yatzy()
    {
        boolean isWon = false;
        for (int i = minScoreOnDice; i <= maxScoreOnDice; i++) {
            final int numberTotest = i;
            List<Integer> numberOfDice = dice.stream().filter(d -> d == numberTotest).collect(Collectors.toList());

            if (numberOfDice.size() == dice.size()) {
                isWon = true;
            }
        }
        
        if (isWon) {
            return 50;
        }
        return 0;
    }

    public int smallStraight()
    {
        // int[] tallies;
        // tallies = new int[6];
        // tallies[d1-1] += 1;
        // tallies[d2-1] += 1;
        // tallies[d3-1] += 1;
        // tallies[d4-1] += 1;
        // tallies[d5-1] += 1;
        // if (tallies[0] == 1 &&
        //     tallies[1] == 1 &&
        //     tallies[2] == 1 &&
        //     tallies[3] == 1 &&
        //     tallies[4] == 1)
        //     return 15;
        return 0;
    }

    public int largeStraight()
    {
        // int[] tallies;
        // tallies = new int[6];
        // tallies[d1-1] += 1;
        // tallies[d2-1] += 1;
        // tallies[d3-1] += 1;
        // tallies[d4-1] += 1;
        // tallies[d5-1] += 1;
        // if (tallies[1] == 1 &&
        //     tallies[2] == 1 &&
        //     tallies[3] == 1 &&
        //     tallies[4] == 1
        //     && tallies[5] == 1)
        //     return 20;
        return 0;
    }

    public int fullHouse()
    {
        // int[] tallies;
        // boolean _2 = false;
        // int i;
        // int _2_at = 0;
        // boolean _3 = false;
        // int _3_at = 0;




        // tallies = new int[6];
        // tallies[d1-1] += 1;
        // tallies[d2-1] += 1;
        // tallies[d3-1] += 1;
        // tallies[d4-1] += 1;
        // tallies[d5-1] += 1;

        // for (i = 0; i != 6; i += 1)
        //     if (tallies[i] == 2) {
        //         _2 = true;
        //         _2_at = i+1;
        //     }

        // for (i = 0; i != 6; i += 1)
        //     if (tallies[i] == 3) {
        //         _3 = true;
        //         _3_at = i+1;
        //     }

        // if (_2 && _3)
        //     return _2_at * 2 + _3_at * 3;
        // else
            return 0;
    }
}



