import org.junit.*;
import static org.junit.Assert.*;

public class YatzyTest {

    
    @Test public void testByNumbers() {
        assertEquals(3, new Yatzy(1,1,1,4,6).byNumber(1));
        assertEquals(4, new Yatzy(2,6,2,4,5).byNumber(2));
        assertEquals(0, new Yatzy(1,2,3,5,6).byNumber(4));
    }

    @Test
    public void one_pair() {
        assertEquals(6, new Yatzy(3,2,3,2,6).scoreMultiples(2));
        assertEquals(10, new Yatzy(5,1,5,4,6).scoreMultiples(2));
        assertEquals(12, new Yatzy(6,6,6,5,5).scoreMultiples(2));
    }

    @Test
    public void triples() {
        assertEquals(9, new Yatzy(3,3,3,4,5).scoreMultiples(3));
        assertEquals(15, new Yatzy(5,3,5,4,5).scoreMultiples(3));
        assertEquals(9, new Yatzy(3,3,3,3,5).scoreMultiples(3));
    }

    @Test
    public void quadruples() {
        assertEquals(12, new Yatzy(3,3,3,3,5).scoreMultiples(4));
        assertEquals(20, new Yatzy(5,5,5,4,5).scoreMultiples(4));
        assertEquals(16, new Yatzy(4,4,4,4,4).scoreMultiples(4));
    }

    @Test
    public void two_Pair() {
        assertEquals(16, new Yatzy(3,3,5,4,5).two_pair());
        assertEquals(16, new Yatzy(3,3,5,5,5).two_pair());
    }

    @Test
    public void chance_scores_sum_of_all_dice() {
        assertEquals(15, new Yatzy(2,3,4,5,1).chance());
        assertEquals(16, new Yatzy(3,3,4,5,1).chance());
    }

    @Test public void yatzy_scores_50() {
        assertEquals(50, new Yatzy(4,4,4,4,4).yatzy());
        assertEquals(50, new Yatzy(6,6,6,6,6).yatzy());
        assertEquals(0, new Yatzy(6,6,6,6,3).yatzy());
    }

    @Test
    public void smallStraight() {
        assertEquals(0, new Yatzy(1,2,3,4,5).smallStraight());
        assertEquals(0, new Yatzy(2,3,4,5,1).smallStraight());
        assertEquals(0, new Yatzy(1,2,2,4,5).smallStraight());
    }

    @Test
    public void largeStraight() {
        assertEquals(0, new Yatzy(6,2,3,4,5).largeStraight());
        assertEquals(0, new Yatzy(2,3,4,5,6).largeStraight());
        assertEquals(0, new Yatzy(1,2,2,4,5).largeStraight());
    }

    @Test
    public void fullHouse() {
        assertEquals(0, new Yatzy(6,2,2,2,6).fullHouse());
        assertEquals(0, new Yatzy(2,3,4,5,6).fullHouse());
    }
}
